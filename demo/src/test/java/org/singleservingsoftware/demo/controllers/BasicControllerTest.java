package org.singleservingsoftware.demo.controllers;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.singleservingsoftware.demo.mappers.TestMapper;
import org.singleservingsoftware.demo.services.BasicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@SpringBootTest
@WebAppConfiguration
public class BasicControllerTest {

    @Autowired
    private WebApplicationContext webApplicationContext;
    private MockMvc mocker;

    @MockBean
    private TestMapper mapper;
    
    @BeforeEach
    public void setup() throws Exception {
        //Ensure mapper is re-mocked for every test
        Mockito.reset(mapper);

        //Set up the MVC mocker to call endpoints
        this.mocker = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
    }

    /**
     * Attempt to hit the random endpoint. Will error if the database is down.
     * @throws Exception 
     */
    @Test
    @Disabled //requires a database to work.
    public void testRandomEndpoint() throws Exception {
        this.mocker.perform(get("/random")).andExpect(status().isOk()).andDo(MockMvcResultHandlers.print());
    }


    /**
     * Attempt to hit the random endpoint. Mocks the database connection so everything else is tested.
     * @throws Exception 
     */
    @Test
    public void testRandomEndpointMockedDatabase() throws Exception {
        //Mock our database call
        when(mapper.retrieveRandomInfo()).thenReturn("0.1234");

        //Call our controller, and now we can expect an explicit value because it's no longer calling
        //the databsae to get a random value.
        this.mocker.perform(get("/random")).andExpectAll(
            MockMvcResultMatchers.status().isOk(),
            MockMvcResultMatchers.content().contentType("text/plain;charset=UTF-8"),

            //The controller adds a "1" to the value to show this is still mocking the whole path.
            MockMvcResultMatchers.content().string("10.1234")
        );
    }

    /**
     * Example of a unit test that logically _should_ fail because the asserted functionality
     * for the addNumberToFrontOfString is wrong.
     */
    @Test
    public void unitTestController() {
        BasicService service = Mockito.mock(BasicService.class);
        BasicController controller = new BasicController(service);
        when(service.getRandomNumber()).thenReturn("0.1234");

        //Bad mock, doesn't match logic of the input method.
        when(service.addNumberToFrontOfString("0.1234")).thenReturn("2.1234");

        ResponseEntity<String> response = controller.getRandomNumber();

        //this is wrong, the actual behavior in a running environment would be "10.1234"
        assertTrue(response.getBody().equals("2.1234"));
    }    
}
