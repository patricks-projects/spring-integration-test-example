package org.singleservingsoftware.demo.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.singleservingsoftware.demo.mappers.TestMapper;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

@SpringBootTest
public class BasicServiceTest {

    @MockBean
    private TestMapper mapper;

    @Test
    public void unitTestServiceMethod() {
        BasicService service = new BasicService(mapper);
        when(mapper.retrieveRandomInfo()).thenReturn("0.1234");

        String result = service.getRandomNumber();

        assertEquals(result, "0.1234");
    }

    @Test
    public void unitTestAddNumberToFrontOfString() {
        BasicService service = new BasicService(mapper);
        String result = service.addNumberToFrontOfString("0.1234");

        assertEquals(result, "10.1234");
    }

    @Test
    public void unitTestAddNumberToFrontOfStringWithNull() {
        BasicService service = new BasicService(mapper);
    
        assertThrows(NullPointerException.class, () -> {
            service.addNumberToFrontOfString(null);
        });
    }


}
