package org.singleservingsoftware.demo;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class DemoApplicationTests {

	@Test
	void contextLoads() {
		// This test intentionally left blank. If it fails, 
        // it means there is a problem with config files.
	}

}
