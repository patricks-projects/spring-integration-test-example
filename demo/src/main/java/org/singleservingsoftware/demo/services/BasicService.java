package org.singleservingsoftware.demo.services;

import org.singleservingsoftware.demo.mappers.TestMapper;
import org.springframework.stereotype.Service;

@Service
public class BasicService {

    private final TestMapper mapper;

    public BasicService(TestMapper mapper) {
        this.mapper = mapper;
    }
    
    public String getRandomNumber() {
        return mapper.retrieveRandomInfo();
    }

    /**
    * This method add a number to the front of the string, specifically a "1"
    */
    public String addNumberToFrontOfString(String input) {
        if (input == null)
            throw new NullPointerException();
            
        return "1" + input;
    }

}
