package org.singleservingsoftware.demo.controllers;

import org.singleservingsoftware.demo.services.BasicService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BasicController {
    
    private final BasicService service;

    BasicController(BasicService service){
        this.service = service;
    }

    @GetMapping("/random")
    public ResponseEntity<String> getRandomNumber() {
        String randomNumber = service.getRandomNumber();
        
        // Add some random logic to test. Yes, this should be in the service,
        // however a lot of non-ideal code exists in the world. We should
        // still test it :)
        randomNumber = service.addNumberToFrontOfString(randomNumber);

        //Return our value
        return new ResponseEntity<String>(randomNumber, HttpStatus.OK);
    }
}
