package org.singleservingsoftware.demo.mappers;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface TestMapper {

    /**
     * Select something to ensure that the database engine is up and running and responding
     * to requests. We don't need to create any tables to prove this.
     * 
     * @return A Long with a random number
     */
    @Select("SELECT random()")
    public String retrieveRandomInfo();
    
}